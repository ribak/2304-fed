const express = require('express') //CommonJs
import { express } from 'express' //EsModules




//default vs specific imports

// default being only 1 import per file, no curly braces for import
// specific import being an import from many available ones and specifiying with a list in curly braces.
const app = express()
const port = 3000;

app.get('/', (req, res) => {
  res.send('Hello World!')
  res.render('page',{data:data})
})


app.listen(port, () => {
  console.log(`Listening on Port:${port}, at http://localhost:${port},/`)
})