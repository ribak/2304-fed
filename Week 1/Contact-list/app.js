const btn = document.getElementById('submit-contact-button')
btn.addEventListener('click', (e) => {
    console.log('button running',e)
    console.log('function 1 running')
})
btn.addEventListener('dblclick', (e) => {
    console.log('DOUBLE CLICKED',e)
})

function addContact() {
    //get the input values from the form
    const firstNameInput = document.getElementById('firstname-input').value;
    const lastNameInput = document.getElementById('lastname-input').value;
    
    //create new h6 element
    const card = document.createElement('h6');

    // string literal
    card.textContent = `${firstNameInput}, ${lastNameInput}`;

    // get container element
    const cardContainer = document.getElementById('card-container');

    //append new h6 element to container
    cardContainer.appendChild(card);

}

function makeContactCard() {
    const firstNameInput = document.getElementById('firstname-input').value;
    const lastNameInput = document.getElementById('lastname-input').value;

    const card = document.createElement('div');
    card.innerHTML= `
    <div class="col-sm-6">
        <div class="card" style="width: 8rem;">
            <div class="card-body">
                <h5 class="card-title">${firstNameInput}, ${lastNameInput}</h5>
            </div>
        </div>
    </div>
    `;
    const cardContainer = document.getElementById('card-container');
    cardContainer.appendChild(card);
}

