
function addContact() {
    //get the input values from the form
    const firstNameInput = document.getElementById('firstname-input').value;
    const lastNameInput = document.getElementById('lastname-input').value;
    
    //create new h6 element
    const card = document.createElement('h6');

    // string literal
    card.textContent = `${firstNameInput}, ${lastNameInput}`;

    // get container element
    const cardContainer = document.getElementById('card-container');

    //append new h6 element to container
    cardContainer.appendChild(card);


}

function makeContactCard() {
    const firstNameInput = document.getElementById('firstname-input').value;
    const lastNameInput = document.getElementById('lastname-input').value;

    const card = document.createElement('div');
    card.innerHTML= `
    <div class="col-sm-6">
        <div class="card" style="width: 8rem;">
            <div class="card-body">
                <h5 class="card-title">${firstNameInput}, ${lastNameInput}</h5>
            </div>
        </div>
    </div>
    `;
    const cardContainer = document.getElementById('card-container');
    cardContainer.appendChild(card);
}
var y = 20;
 
const addContactToLocal = () => {
    let firstNameInput = document.getElementById('firstname-input').value;
    let lastNameInput = document.getElementById('lastname-input').value;
    var contactBook = {
        firstName: 'Elon',
        lastName: 'Musk'
    };

    let myArray = [50,100,150,200,250,300]
    let myObjectArray = [
        {
            id:1312,
            age: 35
        },
        {
            id:12342,
            age: 45
        },
        {
            id:13452,
            age: 78
        },
        {
            id:1252352,
            age: 9
        },
    ];
    // mapped array - iterate thru each entry in an array and run a function on it
    let mappedArray =  myArray.map(num => {
        return num + 25
    })

    let filteredArray = myArray.filter(num => {
        return num >= 200;
    })
    let sumofArray = myArray.reduce((total,num) => {
        return total + num
    })
    let splicedArray = myArray.splice(2,1);
    let mappedObj = myObjectArray.map(person => {
      return {
        age: person.age + 10,
      }
    })
    let flatmapObj = myObjectArray.flatMap(person => {
        return person.age
    })
    console.log('***flatmapObj: ', flatmapObj);
    console.log('mappedObj: ', mappedObj);
    console.log('***splicedArray: ', splicedArray);
    console.log('*** sumofArray: ', sumofArray);
    console.log('*** mappedArray: ', mappedArray);
    console.log('*** filteredArray: ', filteredArray);
    localStorage.setItem('contact-book',JSON.stringify(contactBook));
    localStorage.setItem('Person1',(`${firstNameInput},${lastNameInput}`))
    var myNameObj = JSON.parse(localStorage.getItem('contact-book'));
    
    var fName = myNameObj.firstName
    console.log('***fName: ', fName);
    // annoying pop-up alert - usually only used critical page errors
    // alert(fName)
    variable = 'value'
    var string42 = '42';
    var num42 = "42"

    // if (string42 == num42){
    //     alert('FIRST ALERT')
    // }
    // if( string42 === num42){
    //     alert('SECOND ALERT')
    // }
    if(typeof fName == 'string') {
        if(fName === 'Elon') {
            const cardContainer = document.getElementById('card-container');
            cardContainer.append(fName);
        }
        else {

        }
    }

};
