import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TaskFormComponent } from './components/task-form/task-form.component';
import { TaskTableComponent } from './components/task-table/task-table.component';

const routes: Routes = [
  {path:'', component: TaskFormComponent, title: 'Add Task'},
  {path:'login', component: TaskTableComponent, title: 'Login' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
