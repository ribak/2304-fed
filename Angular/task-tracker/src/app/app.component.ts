import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ITodo } from '../app/models/ITodo';
import { Observable } from 'rxjs'


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'task-tracker';

  tasksany:any
  tasks:ITodo[] = []
  private _url = 'https://jsonplaceholder.typicode.com/todos'

  constructor(private http: HttpClient) {}

  getTasks() {
    this.tasksany = this.http.get(this._url)
  }

  obvserveTodos(): Observable<ITodo[]> {
    return this.http.get<ITodo[]>(this._url)
  }

  subscribeTodos() {
    this.obvserveTodos().subscribe(data => {
      console.log(data)
      this.tasks = data
    })
  }

}
