import { Component } from '@angular/core';
import { Task } from 'src/app/models/Task-interface';

@Component({
  selector: 'app-task-form',
  templateUrl: './task-form.component.html',
  styleUrls: ['./task-form.component.css']
})
export class TaskFormComponent {

  taskTitle!: string
  taskDescription!: string
  taskPriority!: number
  data!:string
  
  addTask(task: Task): void {
    console.log(task)
  }
  
  logData(data:string) {
    console.log(data)
    this.data = data
  }
}
