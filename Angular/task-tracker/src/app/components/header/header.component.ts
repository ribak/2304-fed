import { Component } from '@angular/core';
import { SharedServiceService } from 'src/app/utils/shared-service.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent {
  pretendData!:any
  constructor(private service: SharedServiceService) {}

  getDatafromService(){
    this.pretendData = this.service.appLogger()
  }
    
}
