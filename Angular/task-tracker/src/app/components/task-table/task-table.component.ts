import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-task-table',
  templateUrl: './task-table.component.html',
  styleUrls: ['./task-table.component.css']
})
export class TaskTableComponent {
  @Input() taskTitle!: string;
  
  @Output() logEvent = new EventEmitter();

  logData(data:string): void {
    this.logEvent.emit(data)
  }
}
