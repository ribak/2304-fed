export interface Task {
    taskTitle: string;
    taskDescription: string;
    taskPriority: number;
  }