import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SharedServiceService {

  constructor() { }

  pretendData:string= "lorem ipsum dolor sit amet, consectetur adipiscing"

  appLogger() {
    console.log(this.pretendData)
  }
}
