import React from "react";
import HelloWorld from './HelloWorld'
import ParentComponent from "./ParentComponent";
const App = () => {

    return(
        <div className="app-container">
            <HelloWorld/>
            <ParentComponent/>
        </div>
        
        
        
    )
}

export default App;