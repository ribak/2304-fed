import React, { useEffect } from "react";

const ChildComponent = (parentData,{setChildData}) => {
    const handleChange = (e) => {
        parentData.setChildData(e.target.value)
    }
    return (
        <div className="child-container">
            <h4>Child Component</h4>
            {console.log(parentData.children)}
             <h5>{parentData.data}</h5>

             <h4>Child Input</h4>
             <input type="text" onChange={e => handleChange(e)} />
            <button>Pass Data To parent</button>
        </div>
    )
}

export default ChildComponent;