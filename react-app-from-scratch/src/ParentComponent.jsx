import React,{useState} from "react";
import ChildComponent from "./ChildComponent";
const ParentComponent = () => {
    let tempData;
    const [parentData, setParentData] = useState('Start');
    console.log(parentData)
    
    const [childData, setChildData] = useState('');
    
    const handleDataChange = (e) => {
        tempData = e.target.value
    }

    const handleButtonClick = () => {
        setParentData(tempData)
        console.log('***PARENT DATA',parentData)
    }

    const handleChildChange = (e) => {
        setChildData(e.target.value);
    }
    return (
        <>
        <h3>Parent Component</h3>
        <input type="text" onChange={e => handleDataChange(e)} />
        <button onClick={handleButtonClick}>Pass Data</button>

        <h6>Child Data</h6>
        <p>{childData}</p>

        <ChildComponent data={parentData} setChildData={setChildData}/>

        
        </>
    )
}

export default ParentComponent;