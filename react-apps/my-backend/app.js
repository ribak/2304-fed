const express = require('express');
const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');
const cors = require('cors');
const app = express();
const uri = "mongodb+srv://skabir:PUqB9F5HnMnyRymm@cluster0.njq18fy.mongodb.net/?retryWrites=true&w=majority";
const { User } = require("./models/userSchema.js");

const bcrypt = require("bcrypt");
app.use(cors());

app.use(express.json())

const PORT = 6001;
const SECRET_KEY = 'secret_key';
const validateUser = (password,hash) => {
    let token = false;
    bcrypt.compare(password, hash, function(err,res) {
        if (res){
            token = true
        }
    })
    return token;
}

app.post('/api/users/login', async (req, res) => {
    const userToLogin = req.body.username
    let token;
    console.log('userToLogin : ', userToLogin );
    const foundUser = await User.findOne({userName: userToLogin})
    if (foundUser) {
        console.log('found user ', foundUser.userName)
        console.log('token ', validateUser(req.body.password, foundUser.password));
        if (validateUser(req.body.password, foundUser.password)){
            console.log('token ', validateUser(req.body.password, foundUser.password));
            console.log('correct password', foundUser.password)
            token = jwt.sign({userLoggedIn:foundUser.userName },SECRET_KEY)
            return res.status(299).json(token)
        } 
        // else {
        //     return res.status(297).send({message:'Invalid password'})
        // }
    }
});


app.post('/api/users/newUser', async (req, res) => {
    console.log(req.body)
    const hashedPassword = await bcrypt.hash(req.body.password,12)
    console.log('hashedPassword: ', hashedPassword);
    const newUser = new User({firstName: req.body.firstName, lastName: req.body.lastName, userName: req.body.username, password: hashedPassword});
    console.log(newUser)
    const newAddedUser = await newUser.save()
    return res.status(277).json(newAddedUser)
})

const startServer = async() => {
    try {
        await mongoose.connect(uri)
        app.listen(PORT,() => {console.log(`Listening on ${PORT}`)});
    } catch (error) {
        console.error(error)
        process.exit(1)
    }
}

startServer()