import React from 'react';
import { Redirect, Route, RouteProps } from 'react-router-dom';

function isAuthenticated() {
    const user = sessionStorage.getItem('user');
    
    console.log(user);
    if (user) {
        return user
    }
    else return false;
}

const ProtectedRoute = ({ component: Component, path }: RouteProps) => {
  if (!isAuthenticated) {
    return <Redirect to="/" />;
  }

  return <Route component={Component} path={path} />;
};

export default ProtectedRoute ;