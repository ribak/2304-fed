import React from "react";
import { useState } from "react";
import './CounterApp.css'
const CounterApp = () => {
    const [count,setCount] = useState<number>(0);

    const addCount = () => {
        setCount(count + 1)
    }

    
    return (
        <div className="counter-container">
            <button className="plus-button" onClick={addCount}>+1</button>
            <div className="count">
                {count}
            </div>
        </div>
    )
}

export default CounterApp;