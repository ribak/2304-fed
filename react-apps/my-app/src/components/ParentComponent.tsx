import React,{useState} from 'react';
import { ChildComponent } from './ChildComponent'
export const ParentComponent = () => {
    
    const [data,setData] = useState<string>('')
    const [childData,setChildData] = useState('')

    const handleChange = (e:any) => {
        setChildData(e.target.value)
    }
    return( 
        <div>
        <h3>Parent Component</h3>
        <input type="text" onChange={e => setData(e.target.value)}></input>
        <button onClick={()=> {console.log(setChildData)}}>Log</button>
        <ChildComponent data={data} setChildData={handleChange}/>

        <h6>Child Data: {childData}</h6>
        </div>
    )
}