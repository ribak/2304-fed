import React, { useState } from "react";
import { Button, Container, MenuItem } from "@mui/material";
import { FormControl } from "@mui/material";
import { TextField } from "@mui/material";
import { Select } from "@mui/material";
import "./SignUpForm.css";
import { SIGNUP_URL } from "../routes/userRoutes";
const SignUpComponent = () => {
  const signUp = async (e: React.ChangeEvent<any>) => {
    console.log("Signing up...", firstName, lastName, "User name:", username);
    try {
      await fetch(SIGNUP_URL, {
        method: "POST",
        mode: "cors",
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({
          firstName: firstName,
          lastName: lastName,
          username: username,
          password: password,
        }),
      });
    } catch (error) {
      console.log(error);
    }
    clearAll()
  };

  const handleFirstNameChange = (e: React.ChangeEvent<any>) => {
    setFirstName(e.target.value);
  };

  const handleLastNameChange = (e: React.ChangeEvent<any>) => {
    setLastName(e.target.value);
  };
  const handleUsernameChange = (e: React.ChangeEvent<any>) => {
    setUsername(e.target.value);
  };

  const clearAll = () => {
    setUsername('');
    setFirstName('');
    setLastName('');
    setPassword('');
  }
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [password, setPassword] = useState("");
  const [username, setUsername] = useState("");
  return (
    <div>
      <Container sx={{ marginLeft: 20 }} maxWidth="sm">
        <TextField
          sx={{ padding: 1 }}
          id="first-name"
          value={firstName}
          onChange={(e) => {
            handleFirstNameChange(e);
          }}
          label="First Name"
          variant="outlined"
          size="medium"
        />
        <TextField
          sx={{ padding: 1 }}
          id="last-name"
          value={lastName}
          label="Last Name"
          variant="outlined"
          onChange={(e) => {
            handleLastNameChange(e);
          }}
          size="medium"
        />
        <TextField
          sx={{ padding: 1 }}
          id="username"
          value={username}
          label="Username"
          variant="outlined"
          onChange={(e) => {
            handleUsernameChange(e);
          }}
          size="medium"
        />
        <TextField
          sx={{ padding: 1 }}
          id="password"
          value={password}
          type="password"
          label="Password"
          onChange={(e) => {
            setPassword(e.target.value);
          }}
          variant="outlined"
          size="medium"
        />
      </Container>

      <Container
        sx={{ display: "flex", justifyContent: "center" }}
        className="sign-up-btn"
      >
        <Button id="sign-up-btn" variant="contained" onClick={signUp}>
          Sign Up!
        </Button>
      </Container>
    </div>
  );
};

export default SignUpComponent;
