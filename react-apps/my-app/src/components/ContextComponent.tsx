import React from "react";
import { useContext } from 'react'
import { MyContext } from "../MyContext";
export const ContextComponent = () => {

    const data = useContext(MyContext);
    return (
        <>
        <h4>This is our Context Component</h4>
        <MyContext.Provider value={MyContext}>
        <h6>{data}</h6>
        </MyContext.Provider>
        
        </>
    )
}