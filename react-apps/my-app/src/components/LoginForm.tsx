import { Container, Button, InputAdornment, TextField } from "@mui/material";
import { Redirect } from 'react-router-dom';
import { useHistory } from "react-router-dom";
import AccountCircle from "@mui/icons-material/AccountCircle";
import PasswordIcon from "@mui/icons-material/Password";
import LoginIcon from "@mui/icons-material/Login";
import ClearIcon from "@mui/icons-material/Clear";
import React, { useState, FC } from "react";
import PersonAddIcon from "@mui/icons-material/PersonAdd";
import { Typography } from "@mui/material";
import IconButton from "@mui/material/IconButton";
import SensorOccupiedIcon from "@mui/icons-material/SensorOccupied";
import { Link } from "react-router-dom";
import { SIGNIN_URL } from '../routes/userRoutes'
import "./LoginForm.css";
export const LoginForm: FC = () => {
  const [username, setUsername] = useState<string>("");
  const [password, setPassword] = useState<string>("");
  const history = useHistory();
  const submitLoginForm = async () => {
    console.log("Submitting login form", username, password);

    const data = {
      username: username,
      password: password,
    };

    const options = {
      method: "POST",
      body: JSON.stringify(data),
      headers: { "Content-Type": "application/json" },
    };
    try {
      const response = await fetch(
        SIGNIN_URL,
        options
      );
      if (response.status === 299) {
       const data = await response.json();
       await data
       console.log(data);
       sessionStorage.setItem('user',data);
       history.push('/counter');
      }
      if (response.status === 297) {
        alert('WRONG PASSWORD')
      }
    } catch (error) {

    }
  };

  const clearForm = () => {
    setUsername("");
    setPassword("");
  };
  return (
    <form>
      <Container sx={{ position: "absolute", top: 160 }}>
        <Typography
          sx={{ display: "flex", justifyContent: "center", paddingBottom: 2 }}
          variant="h5"
        >
          Log In
        </Typography>
        <Container sx={{ display: "flex", justifyContent: "center" }}>
          <TextField
            sx={{ padding: 1 }}
            autoComplete="username"
            type="username"
            id="username"
            label="User Name"
            value={username}
            onChange={(e) => {
              setUsername(e.target.value);
            }}
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <AccountCircle />
                </InputAdornment>
              ),
            }}
          />
          <TextField
            sx={{ padding: 1 }}
            autoComplete="current-password"
            type="password"
            id="password"
            label="Password"
            value={password}
            onChange={(e) => {
              setPassword(e.target.value);
            }}
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <PasswordIcon />
                </InputAdornment>
              ),
            }}
          />
        </Container>
        <Container sx={{ display: "flex", justifyContent: "center" }}>
          <Button
            sx={{ padding: 2, margin: 1 }}
            variant="contained"
            endIcon={<LoginIcon />}
            onClick={submitLoginForm}
          >
            Log In
          </Button>
          <Button
            sx={{ padding: 2, margin: 1 }}
            variant="contained"
            endIcon={<ClearIcon />}
            onClick={clearForm}
          >
            Clear
          </Button>
        </Container>
      </Container>
      <Container sx={{ position: "absolute", top: 400, left: 340 }}>
        <Typography
          // sx={{ display: "flex", justifyContent: "center", paddingBottom: 2 }}
          variant="h6"
        >
          Sign Up
        </Typography>
        <IconButton size="large">
            <Link to={'/signup'}><SensorOccupiedIcon/></Link>
        </IconButton>
      </Container>
    </form>
  );
};
