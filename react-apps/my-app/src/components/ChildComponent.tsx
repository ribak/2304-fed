import React,{useEffect, useState} from "react";

interface ChildComponentPropTypes {
    setChildData: (e:any)=> void;
}

export const ChildComponent = ({data,setChildData}:any) => {

    useEffect(()=> {
        console.log(setChildData);
    },[])
    return (
        <>
        <h2>Child Component</h2>
        <h3>{data}</h3>

        <input type="text" onChange={e => setChildData(e)}></input>
        </>
        
    )
}