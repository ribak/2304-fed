import React, { useState } from 'react';
import './App.css';
import CounterApp from './components/CounterApp';
import SignUpComponent from './components/SignUpForm';
import { LoginForm } from './components/LoginForm';
import {ParentComponent} from './components/ParentComponent';
import { ContextComponent } from './components/ContextComponent';
import { MyContext } from './MyContext';
import ProtectedRoute  from '../src/ProtectedRoute';

import { BrowserRouter as Router, Redirect, Switch, Route } from 'react-router-dom'
function App() {
  
  return (
    <div className="App">
      <Router>
        <Switch>

          <Route exact path='/' component={LoginForm} />
          <Route exact path='/signup' component={SignUpComponent} />
          <ProtectedRoute path='/counter' component={CounterApp} />

        </Switch>
      </Router>
    </div>
  );
}

export default App;
